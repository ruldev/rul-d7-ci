# rul-d7-ci

This is a test instance, which forms some of the necessary pipelines for Bitbucket usage.  Use it as a reference.


[![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/ruldev/rul-d7-ci.svg](https://bitbucket.org/ruldev/rul-d7-ci/addon/pipelines/home)
[![Dashboard rul-d7-ci](https://img.shields.io/badge/dashboard-rul_d7_ci-yellow.svg)](https://dashboard.pantheon.io/sites/d9d2044e-3ae9-4ffd-aea2-45aa2c7e155c#dev/code)
[![Dev Site rul-d7-ci](https://img.shields.io/badge/site-rul_d7_ci-blue.svg)](http://dev-rul-d7-ci.pantheonsite.io/)