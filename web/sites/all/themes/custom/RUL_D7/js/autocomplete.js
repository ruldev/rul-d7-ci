var $jq = jQuery.noConflict();

(function($jq) {
  $jq(document).ready(function(){
    $jq( "#search_input" ).autocomplete({
      source: "http://dev7-www.libraries.rutgers.edu/sites/default/themes/RUL_D7/autocomplete.php",
      minLength: 2,
    });
  });    
})(jQuery);