(function ($) {
  var url = 'https://www.iris.rutgers.edu/uhtbin/cgisirsi/0/0/0/1/488/X/BLASTOFF';
  var link_text = 'My Account';
  $(document).ready(function(){
    $('#block-menu-menu-secondary-main-menu .first.leaf').load('dev7-www.libraries.rutgers.edu/sites/default/themes/RUL_D7/checkProxy.php?url=' + encodeURIComponent(url) + '&link_text=' + link_text);
  });
})(jQuery);