<?php
  /**
    Page template for the My Rutgers pages.  It will only apply to pages with the MyRU in the path.
    The application of this template with the path is specified in template.php.  
  **/
  echo '<html><body>';
  //print render($page['content']); 
  //check to see if it is the views generated page for the news (libAnnouncements.html)
  if (arg(1) == 'libAnnouncements.html') {
    //echo '<pre>' . print_r($page['content']['content']['content']['system_main']['main']['#markup'], 1) . '</pre>';
    //echo '<pre>' . print_r($page,1) . '</pre>';
    
    //load the contents of the views generated links into $string variable
    $string = $page['content']['content']['content']['system_main']['main']['#markup'];
    function getLinks($string) {
        $return = array();      //initialize a return array
        $dom = new domDocument; //create a new DOM
        @$dom->loadHTML($string); //load the html of the views generated links into the DOM
        $dom->preserveWhiteSpace = false; //get rid of unwanted whitepace; aka clean up the DOM
        $links = $dom->getElementsByTagName('a'); //get all the links from the views
        //iterate over the $links array to get the values of the href and link text
        foreach ($links as $tag) {
          $return[$tag->getAttribute('href')] = $tag->childNodes->item(0)->nodeValue;
        }
        return $return;
    }
    $urls = getLinks($string);  //call the getLinks function
    echo '<ul>';
    //output the results as li elements
    foreach($urls as $link => $link_text) {
      echo '<li><a href="' . $link . '">' . $link_text . '</a></li>';
    }
    echo '</ul>';
  }
  else {
    //output just the body of the node if it is not the views generated libAnnouncements.html page.
    echo $node->body['und'][0]['value'];
  }
  echo '</body></html>';
?>