<?php
/*
  This is the template file for the user profile pages.
  Dependencies include functions.php (located in the RUL_D7 theme folder), the RUL_custom_code
  module, and user_profile.css (applied via Context/Delta)
*/

require (drupal_get_path('theme', variable_get('theme_default', NULL)) . '/functions.php');
//get the user's info into the $account variable
$account = menu_get_object('user');
if ($account->name == 'suziepoz') {
  drupal_goto('node/2238');
}
else {
  if ($account->name != 'admin') {
    //split multiple phone numbers separated by commas into individual ones.
    $delimit = array(", "); //possible punctuations
    $phone_nums = explodeX($delimit, $user_profile['field_phone']['#items']['0']['value']);
    //put all the associated locations and its info into an array.
    $i = 0;
    foreach ($user_profile['field_profile_library']['#items'] as $lib_loc) {
      $loc[$i]['lib_tid'] = $lib_loc['tid'];
      $i++;
    }
    $count = count($user_profile['field_profile_library']['#items']);
    $i = 0; //reset and reuse $i
    while ($i < $count) {
      //get and store the library name from the taxonomy id
      $term = taxonomy_term_load($user_profile['field_profile_library']['#items'][$i]['tid']);
      $loc[$i]['long_form_name'] = $term->field_long_form['und']['0']['value'];
      if (!empty($term->field_library))
        $loc[$i]['library'] = $term->field_library['und']['0']['value'];
      $loc[$i]['street'] = $term->field_street['und']['0']['value'];
      $loc[$i]['city_state_zip'] = $term->field_city['und']['0']['value'] . ', ' . $term->field_state['und']['0']['value'] . ' ' . $term->field_zip_code['und']['0']['value'];
      if (empty($user_profile['field_building']['#items'][$i]['value']))
        $loc[$i]['bldg'] = $user_profile['field_building']['#items']['0']['value'];
      else
        $loc[$i]['bldg'] = $user_profile['field_building']['#items'][$i]['value'];
      if (empty($user_profile['field_campus']['#items'][$i]['value']))
        $loc[$i]['campus'] = $user_profile['field_campus']['#items']['0']['value'];
      else
        $loc[$i]['campus'] = $user_profile['field_campus']['#items'][$i]['value'];
      $loc[$i]['campus_city'] = $user_profile['field_campus_city']['#items']['0']['value'];
      if (empty($phone_nums[$i]))
        $loc[$i]['phone'] = $phone_nums['0'];
      else
        $loc[$i]['phone'] = $phone_nums[$i];
      $i++;
    }
    $fname = $user_profile['field_first_name']['#items']['0']['value'];
    $lname = $user_profile['field_last_name']['#items']['0']['value'];
    //check for profile pic and set the div widths accordingly for the card_wrapper div
    if (isset($user_profile['user_picture']['#markup']) && !empty($user_profile['user_picture']['#markup']))
      $card_width = 'grid-8';
    else
      $card_width = 'grid-6';
    ?>
    <div id="card_wrapper" class="<?php echo $card_width;?> no-left-margin">
      <div class="nameofperson">
        <div class="nametext">
          <span id="fname"><?php echo $fname; ?></span>
          <span id="lname"><?php echo $lname; ?></span>
          <?php
          if (isset($user_profile['field_orcid_id']['#items']['0']['value'])) {
            echo '<div id="orcid">ORCID: <a href="http://orcid.org/' . trim($user_profile['field_orcid_id']['#items']['0']['value']) . '" target="_blank">' . trim($user_profile['field_orcid_id']['#items']['0']['value']) . '</a></div>';
          }
          if (isset($user_profile['field_researcherid']['#items']['0']['value'])) {
            echo '<div id="researcherid">ResearcherID: <a href="http://www.researcherid.com/rid/' . trim($user_profile['field_researcherid']['#items']['0']['value']) . '" target="_blank">' . trim($user_profile['field_researcherid']['#items']['0']['value']) . '</a></div>';
          }
          ?>
        </div>
      </div><!--end of nameofperson-->
      <div id="visitingcard">
        <div id="visitingcard-wrapper">
          <?php
          //output the profile picture if there is one
          // if (isset($user_profile['user_picture']['#markup']) && !empty($user_profile['user_picture']['#markup'])) {
            // echo '<div id="card-left"><div id="profile_pic">' . $user_profile['user_picture']['#markup'] . '</div></div>';
            // echo '<div id="card-right" class="grid-4">';
          // }
          ?>
          <div id="function-title">
            <?php
            //Output the functional title...loop not really needed but left in due to possible future changes in the field for multiple values
            $counter = 0;
            foreach ($user_profile['field_function_title']['#items'] as $func_title) {
              if ($counter != 0)
                $break = '<br />';
              else
                $break = '';
              $counter++;
              echo $break . $func_title['value'];
            }
            ?>
          </div>
          <?php
          //output the Sub-deparment if any
          if (isset($user_profile['field_sub_department']['#items']['0']['value']))
            echo '<div id="sub-dept">' . $user_profile['field_sub_department']['#items']['0']['value'] . '</div>';
          ?>
          <div id="dept">
            <?php
            /*
              Check to see if the department is the same as functional title (eg. Library faculty).
              If yes, then do nothing.  If not, output the department and perform an additional check
              to see if the unit name is part of the functional title, if so, don't output unit, if not,
              then print out unit name also.
            */
            if ($user_profile['field_function_title']['#items']['0']['value'] != $user_profile['field_department']['#items']['0']['value'])
              echo $user_profile['field_department']['#items']['0']['value'];
              if ((contains($user_profile['field_unit']['#items']['0']['value'], $user_profile['field_department']['#items']['0']['value'])) == FALSE)
                echo ', ' . $user_profile['field_unit']['#items']['0']['value'];
            ?>
          </div>
          <div class="emptydiv"></div>
          <?php
          //iterate through the locations array to output all location info
          $count = count($loc);
          $i = 0;
          while ($i < $count) {
            if ($i > 0)
              echo '<div class="emptydiv"></div>';
            $lib_name = $loc[$i]['long_form_name'] . '<br />';
            if (isset($loc[$i]['library']))
              $lib_lib_loc = $loc[$i]['library'] . '<br />';
            else
              $lib_lib_loc = '';
            $lib_street = $loc[$i]['street'] . '<br />';
            $lib_city_state_zip = $loc[$i]['city_state_zip'];
            $address = $lib_name .  $lib_lib_loc . $lib_street . $lib_city_state_zip;
            echo '<div class="address_container">';
            echo '<div class="address_container_left">';
            echo '<div id="lib_address">' . $address . '</div>';
            echo '<div id="phone">' . $loc[$i]['phone'] . '</div>';
            echo '</div>';
            // if ($i == 0)
              // if (isset($user_profile['user_picture']['#markup']) && !empty($user_profile['user_picture']['#markup']))
                // echo '<div class="address_container_right"><div id="profile_pic">' . $user_profile['user_picture']['#markup'] . '</div></div>';
            echo '</div>';

            $i++;
          }
          ?>
          <div class="emptydiv"></div>
          <div id="email_resume_website_container">
            <div id="email">
              <?php echo '<a href="mailto:' . $account->mail . '"><img src=/sites/default/themes/RUL_D7/images/email_profile.png></a> <a href="mailto:' . $account->mail . '">' . $account->mail . '</a>'; ?>
            </div>
          </div><!--end of email_resume_website_container-->
          <?php
          // if (isset($user_profile['user_picture']['#markup']) && !empty($user_profile['user_picture']['#markup'])) {
            // echo '</div><!--end of card-right-->';
          // }
          ?>
        </div><!--end of visitingcard_wrapper-->
        <?php if (isset($user_profile['user_picture']['#markup']) && !empty($user_profile['user_picture']['#markup'])) ?>
        <div id="profile_pic">
          <?php echo $user_profile['user_picture']['#markup']; ?>
        </div><!--end of profile_pic-->

      </div><!--end of visitingcard-->
    </div><!--end of card_wrapper-->

    <div id="image_social_wrapper" class="grid-2 no-margins">
    <?php
    //print out resume/website/social media links if any
    if (isset($user_profile['field_profile_resume']['#items']['0']['fid']) || isset($user_profile['field_profile_website']['#items']['0']['url']) || isset($user_profile['field_facebook']['#items']['0']['url']) || isset($user_profile['field_twitter']['#items']['0']['url']) || isset($user_profile['field_linkedin']['#items']['0']['url'])) {
      echo '<div id="resume_website_container"><div id="social">';
      if (isset($user_profile['field_profile_resume']['#items']['0']['fid']))
        echo '<div id="resume"><a href="/sites/default/files/' . str_replace("public://", "", $user_profile['field_profile_resume']['#items']['0']['uri']) . '" target="_blank"><img src="/sites/default/themes/RUL_D7/images/pdficon.gif" /></a> <a target="_blank" href="/sites/default/files/' . str_replace("public://", "", $user_profile['field_profile_resume']['#items']['0']['uri']) . '">CV/Resume</a></div>';
      if (isset($user_profile['field_profile_website']['#items']['0']['url']))
        echo '<div id="website"><a href="' . $user_profile['field_profile_website']['#items']['0']['url'] . '" target="_blank"><img src="/sites/default/themes/RUL_D7/images/websiteIcon.png" /></a></div>';
        //echo '<div id="website"><a href="' . $user_profile['field_profile_website']['#items']['0']['url'] . '" target="_blank"><img src="/sites/default/themes/RUL_D7/images/websiteIcon.png" /></a> <a target="_blank" href="' . $user_profile['field_profile_website']['#items']['0']['url'] . '">Website</a></div>';
      if (isset($user_profile['field_facebook']['#items']['0']['url']))
        echo '<div id="fb"><a href="' . clean_url($user_profile['field_facebook']['#items']['0']['url']) . '" target="_blank"><img src="/sites/default/themes/RUL_D7/images/facebook.png" alt="Facebook" title="' . $fname . ' ' . $lname . '\'s Facebook page" /></a></div>';
      if (isset($user_profile['field_twitter']['#items']['0']['url']))
        echo '<div id="twtr"><a href="' . clean_url($user_profile['field_twitter']['#items']['0']['url']) . '" target="_blank"><img src="/sites/default/themes/RUL_D7/images/twitter.png" alt="Twitter" title="' . $fname . ' ' . $lname . '\'s Twitter" /></a></div>';
      if (isset($user_profile['field_linkedin']['#items']['0']['url']))
        echo '<div id="linkedin"><a href="' . clean_url($user_profile['field_linkedin']['#items']['0']['url']) . '" target="_blank"><img src="/sites/default/themes/RUL_D7/images/linkedin.png" alt="LinkedIn" title="' . $fname . ' ' . $lname . '\'s LinkedIn profile" /></a></div>';
      echo '</div></div>';
    }
    ?>
    </div><!--end of image_social_wrapper-->

    <?php
    //print out 1st field if it isn't empty
    if (isset($user_profile['field_profile_field_1']['#items']['0']['value']))
      echo '<div id="profile_field_1">' . $user_profile['field_profile_field_1']['#items']['0']['value'] . '</div>';

    //initialize a counter to set the odd and even number blocks for left/right floats
    $counter = 0;

    echo '<div id="profile_blocks">';
    //print out publications field if it isn't empty
    if (isset($user_profile['field_profile_field_2']['#items']['0']['value'])) {
      //increment the counter and do a check to see if it should have the profile_field_left or profile_field_right class
      $counter++;
      $profile_css = (is_odd($counter) ? 'profile_field_left' : 'profile_field_right');
      echo '<div class="grid-6 ' . $profile_css . '"><h2 class="block-title">Publications</h2>';
      echo '<div id="publications">' . $user_profile['field_profile_field_2']['#items']['0']['value'] . '</div></div>';
    }

    //print out experience field if it isn't empty
    if (isset($user_profile['field_profile_experience']['#items']['0']['value'])) {
      //increment the counter and do a check to see if it should have the profile_field_left or profile_field_right class
      $counter++;
      $profile_css = (is_odd($counter) ? 'profile_field_left' : 'profile_field_right');
      echo '<div class="grid-6 ' . $profile_css . '"><h2 class="block-title">Experience</h2>';
      echo '<div id="experience">' . $user_profile['field_profile_experience']['#items']['0']['value'] . '</div></div>';
    }

    //check and print out the user defined fields.
    $field_extra_fields = field_get_items('user', $account, 'field_profile_fields');
    //stop if there are no user defined fields
    if (!empty($field_extra_fields)) {
      // Extract the field collection item ids
      $ids = array();
      foreach ($field_extra_fields as $field) {
        $ids[] = $field['value'];
      }
      // load and go through and output each field collection field
      $items = field_collection_item_load_multiple($ids);
      foreach ($items as $item) {
        //increment the counter and do a check to see if it should have the profile_field_left or profile_field_right class
        $counter++;
        $profile_css = (is_odd($counter) ? 'profile_field_left' : 'profile_field_right');
        //load and print block title
        $title_field = field_get_items('field_collection_item', $item, 'field_title');
        $title_field_value = array_shift($title_field);
        echo '<div class="grid-6 ' . $profile_css . '"><h2 class="block-title">' . $title_field_value['value'] . '</h2>';
        //load and print block body
        $body_field = field_get_items('field_collection_item', $item, 'field_body');
        $body_field_value = array_shift($body_field);
        echo '<div class="extra_fields">' . $body_field_value['value'] . '</div></div>';
      }
    }
    echo '</div>';
  }
  else { ?>
    <div class="profile"<?php print $attributes; ?>>
      <?php print render($user_profile); ?>
    </div>
  <?php
  }
}
//echo '<!--<pre>'.print_r($user_profile,1).'</pre>-->';
?>