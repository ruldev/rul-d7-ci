<?php print $doctype; ?>
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"<?php print $rdf->version . $rdf->namespaces; ?>>
<head<?php print $rdf->profile; ?>>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
  <?php 
    if (strpos($_SERVER["REQUEST_URI"], '/mobile/'))
      echo "<meta name='viewport' content='width=device-width, initial-scale=1'>\n";    
  ?>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>  
  <?php print $styles; ?>
  <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic" rel='stylesheet' type='text/css' />
  <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel='stylesheet' type='text/css' />
  <?php print $scripts; ?>
  <!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body<?php print $attributes;?>>
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
  </div>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
</html>
