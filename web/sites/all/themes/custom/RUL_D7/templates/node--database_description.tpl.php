<?php require (drupal_get_path('theme', variable_get('theme_default', NULL)) . '/functions.php'); ?>
<article<?php print $attributes; ?>>
  <?php print $user_picture; ?>
  <?php print render($title_prefix); ?>
  <?php if (!$page && $title): ?>
  <header>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  </header>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($display_submitted): ?>
  <footer class="submitted"><?php print $date; ?> -- <?php print $name; ?></footer>
  <?php endif; ?>  
  
  <div<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      //print render($content);
      
      //$wrapper = entity_metadata_wrapper('node', $node);
 
      //custom database description output goes here
      //print '<pre>'.print_r($content,1).'</pre>';
      $note = '';
      if (isset($content['field_access_rights']['0']['#markup'])) {
        switch ($content['field_access_rights']['0']['#markup']) {
          case ('Restricted'):
            $string = '<font color="#a01f18">Rutgers Restricted</font><br />Access';
            $access = 'Restricted';
            $note = 'Off-campus users will be prompted to log in';
            $proxy = true;
            break;
          case ('Unrestricted'):
            $string = 'Unrestricted Access';
            $access = 'Unrestricted';
            $proxy = false;
            break;
          case ('Cancelled - hide connect button'):
          case ('Cancelled - hide connect button, but still show in A-Z list'):
            $string = '';	//No label set if it's cancelled.
            $proxy = false;
            break;
          case ('Restricted - no proxy challenge'):
            $string = '<font color="#a01f18">Rutgers Restricted</font><br />Access';
            $access = 'Restricted-no_proxy';
            $proxy = false;
            break;
          case ('Preview - not listed in A-Z list or subject headings'):
            $string = '<font color="#a01f18">Rutgers Restricted</font><br />Access';
            $access = 'Preview';
            $proxy = false;
            break;
        }
      }
      if (!empty($string)) { ?>
      <div id="connect_url">
        <div class="db_desc_left"><p><b><?php echo $string; ?></b></p></div>
        <div id="connect" class="connect_gif"></div>
        <div class="restricted_note"><?php echo $note; ?></div>
        <script type="text/javascript">
          (function ($) {
          $('#connect').load('<?php echo '/' . drupal_get_path('theme', variable_get('theme_default', NULL)); ?>/connect.php?access=<?php echo $access . '&url=' . urlencode($content['field_url']['#items']['0']['value']); ?>');
          })(jQuery);
        </script>        
      </div><!--end connect_url-->
    <?php } ?>
    
    <?php //print out the connection note if there is one
    if (!empty($content['field_connection_note']['#items']['0']['value'])): ?>
      <div id="connection_note">
        <div class="db_desc_left"><p><b>Connection note</b></p></div>
        <div class="db_desc_right"><?php echo $content['field_connection_note']['#items']['0']['value']; ?></div>
      </div>
    <?php endif; ?>
    
    <?php //print out alternate titles if any
    if (!empty($content['field_alternate_title']['#items']['0']['value'])): /* Print out if not empty */ ?>
      <div id="db_name_alt">
        <div class="db_desc_left"><p><b>Alternate title</b></p></div>
        <div class="db_desc_right">
          <?php
          /**
            * 2-25-11 YL
            *	Parse through alternate titles array and output them.  If there's more than 1, add a comma.
          **/
          $counter = 0; 
          $alt_no = count($content['field_alternate_title']['#items']);
          foreach ($content['field_alternate_title']['#items'] as $value){
            echo '<i>' . $value['value'] . '</i>';
            $counter++; 
            if ($counter < $alt_no) 
              echo ', ';
          }
          ?>
        </div>
      </div><!--end alt_title-->
    <?php endif; ?>
    
    <?php //print out "browser required" field if it's not empty.
    if (!empty($content['field_browser_required']['#items']['0']['value'])):  ?>
      <div id="db_browser">
        <div class="db_desc_left"><p><b>Browser required</b></p></div>
        <div class="db_desc_right"><?php echo $content['field_browser_required']['#items']['0']['value']; ?></div>
      </div><!--end db_browser-->
    <?php endif; ?>
    
    <?php //print out the "plugin required" field if it's not empty.
    if (!empty($content['field_plugins_required']['#items']['0']['value'])):	/* Print out if not empty */ ?>
      <div id="db_plugins">
        <div class="db_desc_left"><p><b>Plugins required</b></p></div>
        <div class="db_desc_right">
          <?php 
          /**	
            * 2-1-11 yl:
            * Step through array to output multiple values if necessary.
            * Setup a counter and get the number of elements in the field_db_plugin array.
            *	Parse through the array and increase counter by 1 each step through.  If the counter is
            *	less then the number of elements in the array, add a comma at the end of the output.
          **/
          $counter = 0; 
          $plugin_no = count($content['field_plugins_required']['#items']);
          foreach ($content['field_plugins_required']['#items'] as $value) {
            echo $content['field_plugins_required'][$counter]['#markup'];
            $counter++; 
            if ($counter < $plugin_no) 
              echo ', ';
          }	
          ?>
        </div>
      </div><!--end db_plugins-->
    <?php endif; ?>

    <?php //print out the description
    if (!empty($content['field_description']['#items']['0']['value'])):	?>
      <div id="db_desc">
        <div class="db_desc_left"><p><b>Description</b></p></div>
        <div class="db_desc_right"><?php echo $content['field_description']['#items']['0']['value']; ?></div>
      </div>
    <?php endif; ?>
    
    <?php //print out the article help link if field is set to yes
    if (!empty($content['field_help']['#items']['0']['value'])):	?>
      <div id="db_help">
        <div class="db_desc_left"><p><b>Help</b></p></div>
        <div class="db_desc_right">
          <?php 
          /**	
            * 2-1-11 YL
            * This will output the help field values as well as check to see if the article help link is set
            * to yes.  If so, output a default help text appended to the end of the help value.
          **/
          echo $content['field_help']['#items']['0']['value'];
          if (($content['field_finding_articles_link']['#items']['0']['value'] == 'yes') || ($content['field_finding_articles_link']['#items']['0']['value'] == '1')): ?>
            <p>For help using these indexes see: <a href="/how_do_i/find_an_article">How do I find an article on my topic?</a></p>
          <?php endif; ?>
        </div>
      </div>
    <?php endif; ?>
    
    <?php //print out 'User tools and features' if it is not empty.
    if (!empty($content['field_user_tools_and_features']['#items']['0']['value'])):	?>
      <div id="db_features">
        <div class="db_desc_left"><p><b>User tools and features</b></p></div>
        <div class="db_desc_right"><?php echo $content['field_user_tools_and_features']['#items']['0']['value']; ?></div>
      </div>
    <?php endif; ?>
    
    <?php //print out 'Dates covered' if it is not empty.
    if (!empty($content['field_dates_covered']['#items']['0']['value'])):	?>
      <div id="db_dates">
        <div class="db_desc_left"><p><b>Dates covered</b></p></div>
        <div class="db_desc_right"><?php echo $content['field_dates_covered']['0']['#markup']; ?></div>
        <?php //echo $content['field_dates_covered']['#items']['0']['value'];  ?>
      </div>
    <?php endif; ?>
    
    <?php //print out 'Updating frequency' if it is not empty.
    if (!empty($content['field_updating_frequency']['#items']['0']['value'])):	?>	
      <div id="db_freq">
        <div class="db_desc_left"><p><b>Updating frequency</b></p></div>
        <div class="db_desc_right"><?php echo $content['field_updating_frequency']['#items']['0']['value']; ?></div>
      </div>
    <?php endif; ?>
        
    <?php //print out 'Sources' if it is not empty.
    if (!empty($content['field_sources']['#items']['0']['value'])):	/* Print out if not empty */ ?>	
      <div id="db_sources">
        <div class="db_desc_left"><p><b>Sources</b></p></div>
        <div class="db_desc_right"><?php echo $content['field_sources']['#items']['0']['value']; ?></div>
      </div>
    <?php endif; ?>
    
    <?php //print out the 'Super collection' fields if it is not empty.
    if (!empty($content['field_super_collection']['#items'])):
      $super_coll_count = count($content['field_super_collection']['#items']); //Get # of elements in the super collection array ?>
      <div id="super_collection">	
        <div class="db_desc_left"><p><b>Super-collection - PREV</b></p></div>
        <div class="db_desc_right">
          <?php 
          if ($super_coll_count > 1) 
            echo '<ul>';
          foreach ($content['field_super_collection']['#items'] as $value) { //Parse for all values in super collection 
            if ($super_coll_count > 1) //If more than 1 element, use list to output
              echo '<li><a href="/' . drupal_lookup_path('alias', "node/" . $value['entity']->nid) . '">' . $value['entity']->title . '</a></li>'; 
            else
              echo '<a href="/' . drupal_lookup_path('alias', "node/" . $value['entity']->nid) . '">' . $value['entity']->title . '</a>';           
          }
          if ($super_coll_count > 1) 
            echo '</ul>';	
          ?>
        </div>
      </div>
    <?php endif; ?>
    
        <?php //print out the 'Super collection' fields if it is not empty.
    if (!empty($content['field_super_collection_']['#items'])):
      $super_coll_count = count($content['field_super_collection_']['#items']); //Get # of elements in the super collection array ?>
      <div id="super_collection">	
        <div class="db_desc_left"><p><b>Super-collection</b></p></div>
        <div class="db_desc_right">
          <?php 
          if ($super_coll_count > 1) 
            echo '<ul>';
          foreach ($content['field_super_collection_']['#items'] as $value) { //Parse for all values in super collection 
            if ($super_coll_count > 1) //If more than 1 element, use list to output
              echo '<li><a href="/' . drupal_lookup_path('alias', "node/" . $value['entity']->nid) . '">' . $value['entity']->title . '</a></li>'; 
            else
              echo '<a href="/' . drupal_lookup_path('alias', "node/" . $value['entity']->nid) . '">' . $value['entity']->title . '</a>';           
          }
          if ($super_coll_count > 1) 
            echo '</ul>';	
          ?>         
        </div>
      </div>
    <?php endif; ?>
    
    <?php //print out the 'Sub collection' fields if it is not empty.
    if (!empty($content['field_sub_collection']['#items'])):
      $sub_coll_count = count($content['field_sub_collection']['#items']); //Get # of elements in the sub collection array ?>
      <div id="sub_collection">	
        <div class="db_desc_left"><p><b>Sub-collection - prev</b></p></div>
        <div class="db_desc_right">
          <?php	
          if ($sub_coll_count > 1) 
            echo '<ul>';
          foreach ($content['field_sub_collection']['#items'] as $value) { //Parse for all values in super collection 
            if ($sub_coll_count > 1)  //If more than 1 element, use list to output
              echo '<li><a href="?q=' . drupal_lookup_path('alias', "node/" . $value['entity']->nid) . '">' . $value['entity']->title . '</a></li>'; 
            else
              echo '<a href="?q=' . drupal_lookup_path('alias', "node/" . $value['entity']->nid) . '">' . $value['entity']->title . '</a>';           
          }
          if ($sub_coll_count > 1) 
            echo '</ul>';
          ?>
        </div>
      </div>
    <?php endif; ?>
    
    <?php //print out the 'Sub collection' fields if it is not empty.
    if (!empty($content['field_sub_collection_']['#items'])):
      $sub_coll_count = count($content['field_sub_collection_']['#items']); //Get # of elements in the sub collection array ?>
      <div id="sub_collection">	
        <div class="db_desc_left"><p><b>Sub-collection</b></p></div>
        <div class="db_desc_right">
          <?php	
          if ($sub_coll_count > 1) 
            echo '<ul>';
          foreach ($content['field_sub_collection_']['#items'] as $value) { //Parse for all values in super collection 
            if ($sub_coll_count > 1)  //If more than 1 element, use list to output
              echo '<li><a href="?q=' . drupal_lookup_path('alias', "node/" . $value['entity']->nid) . '">' . $value['entity']->title . '</a></li>'; 
            else
              echo '<a href="?q=' . drupal_lookup_path('alias', "node/" . $value['entity']->nid) . '">' . $value['entity']->title . '</a>';           
          }
          if ($sub_coll_count > 1) 
            echo '</ul>';
          ?>
        </div>
      </div>
    <?php endif; ?>
    
    <?php 
    /*  Print out the 'Type of coverage' fields if it is not empty.
        Prints out the mark up due to the potential inclusion of the proxy check script in the data.
    */
    if (!empty($content['field_type_of_coverage']['#items']['0']['value'])):  ?>	
      <div id="db_coverage">
        <div class="db_desc_left"><p><b>Type of coverage</b></p></div>
        <div class="db_desc_right"><?php echo $content['field_type_of_coverage']['0']['#markup']; ?></div>
        <?php //echo $content['field_type_of_coverage']['#items']['0']['value']; ?>

      </div>
    <?php endif; ?>
    
    <?php 
    /*  Print out the 'Print counterpart or related resources' fields if it is not empty.
        Prints out the mark up due to the potential inclusion of the proxy check script in the data.
    */
    if (!empty($content['field_print_counterpart_or_relat']['#items']['0']['value'])):  ?>		
      <div id="db_print">	
        <div class="db_desc_left"><p><b>Print counterpart or<br />related resources</b></p></div>
        <div class="db_desc_right"><?php echo $content['field_print_counterpart_or_relat']['0']['#markup']; ?></div>
        <?php //echo $content['field_print_counterpart_or_relat']['#items']['0']['value']; ?>
      </div>
    <?php endif; ?>
    
    <?php //print out the funder field if it is not empty
    if (!empty($content['field_funder']['#items']['0']['value'])):
      //check to see which funder it is and set appropriate $url and $img vars
      switch ($content['field_funder']['#items']['0']['value']) {
        case('jerseyclicks'):
          $url = '<a href="http://www.jerseyclicks.org">JerseyClicks</a>';
          $img = '<a href="http://www.jerseyclicks.org" style="border:none"><img src="/' . drupal_get_path('theme', variable_get('theme_default', NULL)) . '/images/jerseyclicks.gif" /></a>';
          $spacer = TRUE;
          break;
        case('njki'):
          $url = '<a href="http://njki.njstatelib.org">NJKI - New Jersey Knowledge Initiative</a>';
          $img = '<a href="http://njki.njstatelib.org" style="border:none"><img src="/' . drupal_get_path('theme', variable_get('theme_default', NULL)) . '/images/njki_nav.jpg" /></a>';
          $spacer = TRUE;
          break;
        case('prev_njki'):
          $url = 'Previously funded by the <a href="http://njki.njstatelib.org/">NJKI - New Jersey Knowledge Initiative</a>';
          $spacer = FALSE;
          break;
        case('vale'):
          $url = '<a href="http://www.valenj.org">VALE - Virtual Academic Library Environment of New Jersey</a>';
          $img = '<a href="http://www.valenj.org" style="border:none"><img src="/' . drupal_get_path('theme', variable_get('theme_default', NULL)) . '/images/vale.png" /></a>';
          $spacer = TRUE;
          break;
      }
      if (isset($spacer)) {
        if ($spacer == TRUE) {
          $pad_top = 'padding-top:30px';
        }
        else {
          $pad_top = '';
        }
      }
      ?>  
      <div id="db_thanksto">
        <div class="db_desc_left" style="<?php if (isset($pad_top)) echo $pad_top; ?>"><p><b>Funder</b></p></div>
        <div class="db_desc_right"><?php if (isset($pad_top)) echo '<div style="float:left;padding-right:30px;' . $pad_top . '">' . $url . '</div>' . '<div style="float:left">' . $img . '</div>'; ?></div>
      </div>
    <?php endif; ?>    
    
    <?php //print out the 'Producer/content provider' fields if it is not empty.
    if (!empty($content['field_producer_content_provider']['#items']['0']['value'])): ?>			
      <div id="db_producer">
        <div class="db_desc_left"><p><b>Producer/content provider</b></p></div>
        <div class="db_desc_right"><?php echo $content['field_producer_content_provider']['#items']['0']['value']; ?></div>
      </div>
    <?php endif; ?>
    
    <?php //print out the 'Vendor/electronic presentation provider' fields if it is not empty.
    if (!empty($content['field_vendor_electronic_presenta']['#items']['0']['value'])):  ?>
      <div id="db_vendor">
        <div class="db_desc_left"><p><b>Vendor/electronic presentation provider</b></p></div>
        <div class="db_desc_right"><?php echo $content['field_vendor_electronic_presenta']['#items']['0']['value']; ?></div>
      </div>
    <?php endif; ?>

    <div style="clear:both"><hr class="major" /></div>
    
    <?php //functions for taxonomy displays
    $delimit = Array(".", "!", " ", "?", ";", "&", "/", "-", ","); // Break elements that could exist in a term
    $parents_array = Array(
      'Arts and Humanities' => 'arts_hum',
      'General and Multidisciplinary' => 'general',
      'Science, Technology, Engineering and Math' => 'sciences',
      'Social Sciences' => 'social_sciences',
      'Health Sciences' => 'health',
      'Business' => 'business',
      'Law' => 'law'
    );
    ?>
    <?php //print out the Core subjects if it is not empty
    if (!empty($content['field_core']['#items'])):  ?>
    <div id="db_core">
      <div class="db_desc_left"><p><b>Core Subject(s)</b></p></div>
      <div class="db_desc_right"><p>
      <?php 
      foreach ($content['field_core']['#items'] as $key => $core) {
        $parents = taxonomy_get_parents($core['tid']);  //get the parents of the term
        //if parent terms exist, call the subject_link function with only the first parent
        if ($parents)
          $link = subject_link($delimit, $parents_array, $core['taxonomy_term']->name, array_shift($parents));
        else
          //no parents so just send the subject_link function NULL in place of the parent
          $link = subject_link($delimit, $parents_array, $core['taxonomy_term']->name, NULL);
        //links are stored in an array because there might be duplicates
        $core_subjects[$key] = $link;
      }
      //remove the duplicates from the array and print out the values.
      $core_subjects = array_unique($core_subjects);
      foreach ($core_subjects as $key => $value) {
        if ($key > 0)
          echo '; ';
        echo $value;
      }
      ?>
      </p></div>
    </div>
    <?php endif; ?>
    
    <?php //print out the related subjects if it is not empty
    if (!empty($content['field_related']['#items'])): ?>
      <div id="db_related">
        <div class="db_desc_left"><p><b>Related Subject(s)</b></p></div>
        <div class="db_desc_right"><p>
        <?php
        foreach ($content['field_related']['#items'] as $key => $related) {
          $parents = taxonomy_get_parents($related['tid']); //get the parents of the term
          //if parent terms exist, call the subject_link function with only the first parent
          if ($parents)
            $link = subject_link($delimit, $parents_array, $related['taxonomy_term']->name, array_shift($parents));
          else
            //no parents so just send the subject_link function NULL in place of the parent
            $link = subject_link($delimit, $parents_array, $related['taxonomy_term']->name, NULL); 
          //links are stored in an array because there might be duplicates
          $related_subjects[$key] = $link;     
        }
        //remove the duplicates from the array and print out the values.
        $related_subjects = array_unique($related_subjects);
        foreach ($related_subjects as $key => $value) {
          if ($key > 0)
            echo '; ';
          echo $value;
        }        
        ?>
        </p></div>
      </div>
    <?php endif; ?>
    
    <?php //print out the supplementary subjects if it is not empty
    if (!empty($content['field_supplementary']['#items'])): ?>
      <div id="db_supplementary">
        <div class="db_desc_left"><p><b>Supplementary Subject(s)</b></p></div>
        <div class="db_desc_right">
        <?php 
        foreach ($content['field_supplementary']['#items'] as $key => $supple) {
          $parents = taxonomy_get_parents($supple['tid']);  //get the parents of the term
          //if parent terms exist, call the subject_link function with only the first parent
          if ($parents)
            $link = subject_link($delimit, $parents_array, $supple['taxonomy_term']->name, array_shift($parents));
          else
            //no parents so just send the subject_link function NULL in place of the parent
            $link = subject_link($delimit, $parents_array, $supple['taxonomy_term']->name, NULL);     
          //links are stored in an array because there might be duplicates          
          $supplementary_subjects[$key] = $link;
        }
        //remove the duplicates from the array and print out the values.
        $supplementary_subjects = array_unique($supplementary_subjects);
        foreach ($supplementary_subjects as $key => $value) {
          if ($key > 0)
            echo '; ';
          echo $value;
        }                
        ?>
        </div>
      </div>
    <?php endif; ?>
    
    <?php ?>
  </div>
  
  <div class="clearfix">
    <?php if (!empty($content['links'])): ?>
      <nav class="links node-links clearfix"><?php print render($content['links']); ?></nav>
    <?php endif; ?>

    <?php print render($content['comments']); ?>
  </div>
</article>