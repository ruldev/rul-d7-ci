export PATH=$PATH:$HOME/bin
export PR_NUMBER=
export CI_BRANCH=master
export CI_BUILD_NUMBER=7
export DEFAULT_SITE='ruldev/rul-d7-ci'
export CI_PR_URL='https://bitbucket.org/ruldev/rul-d7-ci/pull-requests/'
export CI_PROJECT_USERNAME='ruldev'
export CI_PROJECT_REPONAME='rul-d7-ci'
export DEFAULT_ENV='dev'
export TERMINUS_HIDE_UPDATE_MESSAGE=1
export TERMINUS_SITE='rul-d7-ci'
export TERMINUS_ENV='dev'
export DEFAULT_BRANCH='master'
export BEHAT_ADMIN_PASSWORD=$(openssl rand -base64 24)
export BEHAT_ADMIN_USERNAME=pantheon-ci-testing-$CI_BUILD_NUMBER
export BEHAT_ADMIN_EMAIL=no-reply+ci-$CI_BUILD_NUMBER@getpantheon.com
export MULTIDEV_SITE_URL='https://dev-rul-d7-ci.pantheonsite.io/'
export DEV_SITE_URL='https://dev-rul-d7-ci.pantheonsite.io/'
export TEST_SITE_URL='https://test-rul-d7-ci.pantheonsite.io/'
export LIVE_SITE_URL='https://live-rul-d7-ci.pantheonsite.io/'
export ARTIFACTS_DIR='artifacts'
export ARTIFACTS_FULL_DIR='/tmp/artifacts'
